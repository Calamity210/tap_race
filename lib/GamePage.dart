import 'dart:async';

import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tap_race/HomePage.dart';



class GamePage extends StatefulWidget {
  GamePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<GamePage> {
  int _counter = 0;
  Timer _timer;
  int _start = 10; 

  void startTimer() {
  const oneSec = const Duration(seconds: 1);
  _timer = new Timer.periodic(
    oneSec,
    (Timer timer) => setState(
      () {
        if (_start < 1) {
          timer.cancel();
           Navigator.push(
					context,
					MaterialPageRoute(builder: (context) => MyHomePage(title: "Tap Race")),
				);
        
        } else {
          _start = _start - 1;
        }
      },
    ),
  );
}

@override
void dispose() {
  _timer.cancel();
  super.dispose();
}

  void _incrementCounter() async{
    setState(() {
      
      _counter++;
    });

    SharedPreferences prefs = await SharedPreferences.getInstance();

if (prefs.getInt('highscore') == null){
      prefs.setInt('highscore', _counter);
    }
    else if(_counter > prefs.getInt('highscore') ){
      prefs.setInt('highscore', _counter);
    }
  }

  @override
  void initState() {
    super.initState();
    startTimer();
  }

  @override
  Widget build(BuildContext context) {
    
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/game_page.png")
          )
        ),
        child: Stack(
          children: <Widget>[


            Padding(
              padding: const EdgeInsets.only(top: 100.0),
              child: Align(
                alignment: Alignment.topCenter,
                child: Text("$_start",
                style: TextStyle(color: Colors.black,
                fontSize: 40
                ),),
              ),
            ),

            Align(
              alignment: Alignment.center,
              child: GestureDetector(
                onTap: _incrementCounter,
              child:CircleAvatar(
                backgroundColor: Colors.transparent,
                radius: 130,
              ),
            ),
            ),

            Padding(
              padding: const EdgeInsets.only(bottom: 50.0),
              child: Align(
                alignment: Alignment.bottomCenter,
                child: Text("Taps: "+_counter.toString(),
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 50
                ),
                ),
              ),
            )
          ],
        ),
      ),
      
    );
  }
}