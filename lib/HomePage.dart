import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tap_race/GamePage.dart';

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  TextEditingController _nameController = new TextEditingController();

  int _highscore = 0;

  @override
  void initState(){
    super.initState();

    gethighscore();
  }

  void gethighscore() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();

    setState(() {
      _highscore = prefs.getInt('highscore');
    });
  }
  
  @override
  Widget build(BuildContext context) {
    
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/home_page.png"),
            fit: BoxFit.fill
          )
        ),
        child: Stack(
          children: <Widget>[
            
            Align(
              alignment: Alignment.center,
              child: Container(        
                width: MediaQuery.of(context).size.width * 0.9,        
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(28.0),
                color: Colors.white,

                ),
                child: TextField(
                                    cursorColor: Colors.white,
                                    textInputAction: TextInputAction.done,
                                    textCapitalization: TextCapitalization.words,
                                    style: TextStyle(
                                        fontSize: 18, color: Colors.black),
                                    controller: _nameController,
                                    decoration: InputDecoration(
                                        enabledBorder: OutlineInputBorder(
                                          borderRadius:
                                              BorderRadius.circular(25.0),
                                          borderSide: BorderSide(
                                              color: Colors.black, width: 2),
                                        ),
                                        border: OutlineInputBorder(
                                          borderRadius:
                                              BorderRadius.circular(35.0),
                                          borderSide: BorderSide(
                                              color: Colors.black, width: 2.0),
                                        ),
                                        labelText: 'Name',
                        labelStyle:
                                TextStyle(color: Colors.black)),
                              ),
              ),
      
            ),

            Padding(
              padding: const EdgeInsets.only(top: 400.0),
              child: Align(
                alignment: Alignment.center,
                child: ButtonTheme(
                  minWidth: 150.0,
                  height: 60.0,
                  child: RaisedButton(
                    color: Colors.black,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(25.0),
                      side: BorderSide(
                        width: 6,
                        color: Colors.white,
                         style: BorderStyle.solid
                         )
                       ),
                    child: Text("Start",
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 20
                    )
                    ),
                    onPressed: (){
                      Navigator.push(
					context,
					MaterialPageRoute(builder: (context) => GamePage(title: "Tap Race")),
				);
                    },
                  ),
                ),
              ),
            ),

            Padding(
              padding: const EdgeInsets.only(bottom: 60.0),
              child: Align(
                alignment: Alignment.bottomCenter,
                child: Text(
                  "Highscore: " +(_highscore == null ? "0" : _highscore.toString()),
                  textAlign: TextAlign.left,
                  style: TextStyle(
                    
                    color: Colors.lime,
                    fontWeight: FontWeight.bold,
                    fontSize: 25
                  ),
                ),
              ),
            )

          ],
        ),
      ),
    );
  }
}
