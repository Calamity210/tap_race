import 'package:flutter/material.dart';
import 'package:tap_race/HomePage.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Tap Race',
      theme: ThemeData(
       
        primarySwatch: Colors.brown,
      ),
      home: MyHomePage(title: 'Tap Race'),
    );
  }
}


